.PHONY: chaotic-text-in-osns.pdf all clean
all: chaotic-text-in-osns.pdf

chaotic-text-in-osns.pdf: chaotic-text-in-osns.tex
	latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make $<

clean:
	latexmk -CA
